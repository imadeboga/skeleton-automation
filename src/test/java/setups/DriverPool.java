package setups;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by madeboga on 9/13/17.
 */
public class DriverPool {
    private static AppiumDriver<WebElement> driverInstance;
    private static WebDriverWait waitInstance;

    public static WebDriverWait getWaitInstance() {
        return waitInstance;
    }

    public static void setWaitInstance(WebDriverWait waitInstance) {
        DriverPool.waitInstance = waitInstance;
    }

    public static AppiumDriver<WebElement> getDriverInstance() {
        return DriverPool.driverInstance;
    }

    public void setDriverInstance(AppiumDriver<WebElement> iosInstance) {
        DriverPool.driverInstance = iosInstance;
    }
}
