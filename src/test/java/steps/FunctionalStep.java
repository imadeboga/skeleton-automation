package steps;

import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by madeboga on 9/13/17.
 */
public class FunctionalStep extends BaseStep {

    public void clearElement(String name){
        getDriverInstance().findElementByName(name).clear();
    }
    public void clearElement(By by){
        getDriverInstance().findElement(by).clear();
    }
    public void sendKeysElement(String name,String keys){
        getDriverInstance().findElementByName(name).sendKeys(keys);
        getDriverInstance().getKeyboard();
        getDriverInstance().hideKeyboard();
    }
    public void sendKeysElement(By by, String keys){
        getDriverInstance().findElement(by).sendKeys(keys);
        getDriverInstance().getKeyboard();
        getDriverInstance().hideKeyboard();
    }
    public void tapElement(String name){
        getDriverInstance().findElementByName(name).click();
    }
    public void tapElement(By by){
        getDriverInstance().findElement(by).click();
    }
    public WebElement getElement(String name){
        return getDriverInstance().findElementByName(name);
    }
    public WebElement getElement(By by){
        return getDriverInstance().findElement(by);
    }
    public void waitForElementToBeVisible(String name) {
        getWaitInstance().until(ExpectedConditions.visibilityOfElementLocated(By.id(name)));
    }
    public void waitForElementToBeVisible(By by) {
        getWaitInstance().until(ExpectedConditions.visibilityOfElementLocated(by));
    }
    public void waitForElementToBeEnable(By by) {
        getWaitInstance().until(ExpectedConditions.elementToBeClickable(by));
    }
    public void waitForElementToBeDisplayed(By by) {
        getWaitInstance().until(ExpectedConditions.elementToBeClickable(by)).isDisplayed();
    }
    public void waitForTime(long time) throws InterruptedException {
        Thread.sleep(time);
    }

    public void swipeVertical(double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = getDriverInstance().manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(getDriverInstance()).press(anchor, startPoint).waitAction(duration).moveTo(0,endPoint-startPoint).release().perform();
    }

    public void swipeHorizontal(double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = getDriverInstance().manage().window().getSize();
        int anchor = (int) (size.height * anchorPercentage);
        int startPoint = (int) (size.width * startPercentage);
        int endPoint = (int) (size.width * finalPercentage);
        new TouchAction(getDriverInstance()).press(startPoint, anchor).waitAction(duration).moveTo(endPoint-startPoint,0).release().perform();
    }
    public void scrollTopDown() throws Exception {
        swipeVertical(0.7,0.3,0.5,3000);
    }
    public void scrollDownTop() throws Exception {
        swipeVertical(0.3,0.7,0.5,3000);
    }
    public void swipeRightLeft() throws Exception {
        swipeHorizontal(0.7,0.03,0.7,3000);
    }
    public void swipeLeftRight() throws Exception {
        swipeHorizontal(0.03,0.7,0.7,3000);
    }
}
